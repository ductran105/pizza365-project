package com.example.drink_api.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.drink_api.models.CDrink;

@RestController
@CrossOrigin
public class CDrinkController {
  @GetMapping("/drinks")
  public ArrayList<CDrink> getDrinks() {

    ArrayList<CDrink> drinks = new ArrayList<CDrink>();
    CDrink drink1 = new CDrink("TRATAC", "Trà tắc", 10000, null, null, null);
    CDrink drink2 = new CDrink("COCA", "Cocacola", 15000, null, null, null);
    CDrink drink3 = new CDrink("PEPSI", "Pepsi", 15000, null, null, null);
    CDrink drink4 = new CDrink("LAVIE", "Lavie", 5000, null, null, null);
    CDrink drink5 = new CDrink("TRASUA", "Trà sữa trân châu", 40000, null, null, null);
    CDrink drink6 = new CDrink("FANTA", "Fanta", 15000, null, null, null);

    drinks.add(drink1);
    drinks.add(drink2);
    drinks.add(drink3);
    drinks.add(drink4);
    drinks.add(drink5);
    drinks.add(drink6);

    return drinks;
  }
}
