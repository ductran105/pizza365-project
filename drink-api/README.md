**Tất cả thông tin đồ uống phải lấy về được từ server qua restAPI và spring boot service**

```java
public class CDrink {
  private String maNuocUong;
  private String tenNuocUong;
  private int donGia;
  private String ghiChu;
  private Date ngayTao;
  private Date ngayCapNhat;

  public CDrink(String maNuocUong, String tenNuocUong, int donGia, String ghiChu, Date ngayTao, Date ngayCapNhat) {
    this.maNuocUong = maNuocUong;
    this.tenNuocUong = tenNuocUong;
    this.donGia = donGia;
    this.ghiChu = ghiChu;
    this.ngayTao = ngayTao;
    this.ngayCapNhat = ngayCapNhat;
  }
}
```
**API bằng Java Spring Boot với RestController là Class CDrinkController class 
Trả về một bảng danh sách các đồ uống. (Trả về ArrayList<CDrink> với CDrink là 1 Class chứa các thuộc tính của đồ uống)**
```java
public class CDrinkController {
  @GetMapping("/drinks")
  public ArrayList<CDrink> getDrinks() {

    ArrayList<CDrink> drinks = new ArrayList<CDrink>();
    CDrink drink1 = new CDrink("TRATAC", "Trà tắc", 10000, null, null, null);
    CDrink drink2 = new CDrink("COCA", "Cocacola", 15000, null, null, null);
    CDrink drink3 = new CDrink("PEPSI", "Pepsi", 15000, null, null, null);
    CDrink drink4 = new CDrink("LAVIE", "Lavie", 5000, null, null, null);
    CDrink drink5 = new CDrink("TRASUA", "Trà sữa trân châu", 40000, null, null, null);
    CDrink drink6 = new CDrink("FANTA", "Fanta", 15000, null, null, null);

    drinks.add(drink1);
    drinks.add(drink2);
    drinks.add(drink3);
    drinks.add(drink4);
    drinks.add(drink5);
    drinks.add(drink6);

    return drinks;
  }
}
```