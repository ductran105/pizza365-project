package com.example.menu_api.models;

public class CMenu {
  private char kichCo;
  private int duongKinh;
  private int suonNuong;
  private int salad;
  private int nuocNgot;
  private int thanhTien;

  public CMenu(char size, int duongKinh, int suon, int salad, int soLuongNuocNgot, int donGia) {
    this.kichCo = size;
    this.duongKinh = duongKinh;
    this.suonNuong = suon;
    this.salad = salad;
    this.nuocNgot = soLuongNuocNgot;
    this.thanhTien = donGia;
  }

  // method getter and setter
  public char getKichCo() {
    return kichCo;
  }

  public void setKichCo(char size) {
    this.kichCo = size;
  }

  public int getDuongKinh() {
    return duongKinh;
  }

  public void setDuongKinh(int duongKinh) {
    this.duongKinh = duongKinh;
  }

  public int getSuonNuong() {
    return suonNuong;
  }

  public void setSuonNuong(int suon) {
    this.suonNuong = suon;
  }

  public int getSalad() {
    return salad;
  }

  public void setSalad(int salad) {
    this.salad = salad;
  }

  public int getThanhTien() {
    return thanhTien;
  }

  public void setThanhTien(int donGia) {
    this.thanhTien = donGia;
  }

  public int getNuocNgot() {
    return nuocNgot;
  }

  public void setNuocNgot(int soLuongNuocNgot) {
    this.nuocNgot = soLuongNuocNgot;
  }
}
