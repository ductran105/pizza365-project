***API bằng Java Spring Boot với RestController là Class CDailyCampaign, tùy ngày mà trả ra 1 khuyến mại***


```java
private static final Map<DayOfWeek, String> promotions = new HashMap<>();

  static {
    promotions.put(DayOfWeek.MONDAY, "Thứ hai: Mua 1 tặng 1");
    promotions.put(DayOfWeek.TUESDAY, "Thứ ba: Tặng tất cả khách hàng một phần bánh ngọt");
    promotions.put(DayOfWeek.WEDNESDAY, "Thứ tư: Giảm giá 20% cho tất cả các sản phẩm");
    promotions.put(DayOfWeek.THURSDAY, "Thứ năm: Tặng một ly nước miễn phí");
    promotions.put(DayOfWeek.FRIDAY, "Thứ sáu: Miễn phí vận chuyển");
    promotions.put(DayOfWeek.SATURDAY, "Thứ bảy: Giảm giá 10% cho đơn hàng trên 500k");
    promotions.put(DayOfWeek.SUNDAY, "Chủ nhật: Tặng một voucher mua sắm 100k");
  }
```

```java
@GetMapping("/daily-promotion")
  public String getDailyPromotion() {
    DayOfWeek today = LocalDate.now().getDayOfWeek();
    return promotions.getOrDefault(today, "Không có khuyến mại cho ngày này");
  }
```