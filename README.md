# Pizza 365 Restaurant

## 📄 Description

- ### Home page cửa hàng Pizza 365, cho phép khách đặt hàng

## ✨ Feature

- Chọn combo size pizza:
Gọi API để lấy thông tin combo size pizza từ server
  ![DatHang](front-end/images/comboSize.png)
- Chọn loại pizza:
Gọi API để lấy thông tin loại pizza từ server
  ![DatHang](front-end/images/pizzaType.png)
- Chọn nước và điền thông tin giao hàng:
  ![DatHang](front-end/images/infoGuess.png)
- Kiểm tra đơn hàng:
  ![DatHang](front-end/images/checkOrder.png)
- Xác nhận đặt thành công
  ![DatHang](front-end/images/successOrder.png)

## 🧱 Technology

- Front-end:
  - Bootstrap 4
  - Javascript
  - Jquery 3
  - Ajax

- Back-end:
  
  - Spring Boot
  - MySQL
  - Hibernate
  - JPA
