package com.example.daily_campaign_api.controllers;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CDailyCampaign {
  private static final Map<DayOfWeek, String> promotions = new HashMap<>();

  static {
    promotions.put(DayOfWeek.MONDAY, "Thứ hai: Mua 1 tặng 1");
    promotions.put(DayOfWeek.TUESDAY, "Thứ ba: Tặng tất cả khách hàng một phần bánh ngọt");
    promotions.put(DayOfWeek.WEDNESDAY, "Thứ tư: Giảm giá 20% cho tất cả các sản phẩm");
    promotions.put(DayOfWeek.THURSDAY, "Thứ năm: Tặng một ly nước miễn phí");
    promotions.put(DayOfWeek.FRIDAY, "Thứ sáu: Miễn phí vận chuyển");
    promotions.put(DayOfWeek.SATURDAY, "Thứ bảy: Giảm giá 10% cho đơn hàng trên 500k");
    promotions.put(DayOfWeek.SUNDAY, "Chủ nhật: Tặng một voucher mua sắm 100k");
  }

  @GetMapping("/daily-promotion")
  public String getDailyPromotion() {
    DayOfWeek today = LocalDate.now().getDayOfWeek();
    return promotions.getOrDefault(today, "Không có khuyến mại cho ngày này");
  }
}
