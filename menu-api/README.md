**Tất cả thông tin Size (L, M, S), đường kính,.., giá tiền phải lấy về được từ server qua restAPI và sprint boot service**

```java
public class CMenu {
  private char kichCo;
  private int duongKinh;
  private int suonNuong;
  private int salad;
  private int nuocNgot;
  private int thanhTien;

  public CMenu(char size, int duongKinh, int suon, int salad, int soLuongNuocNgot, int donGia) {
    this.kichCo = size;
    this.duongKinh = duongKinh;
    this.suonNuong = suon;
    this.salad = salad;
    this.nuocNgot = soLuongNuocNgot;
    this.thanhTien = donGia;
  }
}
```
**API bằng Java Spring Boot với RestController là Class CComboMenu class 
Trả về một bảng danh sách các menu. (Trả về ArrayList<CMenu> với CMenu là 1 Class chứa các thuộc tính của Menu)**

```java
public class CComboMenu {
  @GetMapping("/combomenu")
  public ArrayList<CMenu> getComboMenu() {

    ArrayList<CMenu> menu = new ArrayList<CMenu>();
    CMenu sizeS = new CMenu('S', 20, 2, 200, 2, 150000);
    CMenu sizeM = new CMenu('M', 25, 4, 300, 3, 200000);
    CMenu sizeL = new CMenu('L', 30, 8, 500, 4, 250000);

    menu.add(sizeS);
    menu.add(sizeM);
    menu.add(sizeL);

    return menu;
  }
}

```
