package com.example.daily_campaign_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DailyCampaignApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DailyCampaignApiApplication.class, args);
	}

}
